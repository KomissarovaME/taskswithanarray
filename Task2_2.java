import java.util.Arrays;

public class Task2_2 {
    public static void main(String[] args) {
        int[] x={3, 9, 15, 5, 20, 25, 16};
        int[] a=new int[10];
        int[] b=new int[10];
        int[] c= new int[10];
        writingNumbers(x, a, b, c);

    }
/*
*Метод для нахождения чисел кратных 5,3, 5 и 3.
*/
    private static void writingNumbers(int[] x, int[] a, int[] b, int[] c) {
        int s=-1;
        int z=-1;
        int w=-1;
        for(int i=0; i<x.length ;i++){
            if(x[i]%3==0){
                s++;
                a[s]= x[i];
            }
            if (x[i]%5==0){
                z++;
                b[z]= x[i];
            }
            if(x[i]%5==0&x[i]%3==0){
                w++;
                c[w]= x[i];
            }
        }
        System.out.printf("Кратные 3: %S  \nКратные 5: %S  \nКратные 5 и 3: %S", Arrays.toString(a),Arrays.toString(b),Arrays.toString(c));
    }
}
/*
*Arrays.toString(a) метод возвращающий в строчку эллементы массива.
*/