import java.util.ArrayList;

public class Task2_1 {
    public static void main(String[] args) {
        int[] x={3, 5, 7, 9, 10, 15, 21};
        ArrayList<Integer> a=new ArrayList<Integer>();
        ArrayList<Integer> b=new ArrayList<>();
        ArrayList<Integer> c =new ArrayList<>();
        A(x, a); B(x, b); C(x, c);
        System.out.printf(" %s \n %s \n %s", a, b ,c );
    }

    private static void C(int[] x, ArrayList<Integer> c) {
        for(int i=0; i<x.length ;i++){
            if((x[i]%3==0)&&(x[i]%5==0)){c.add(x[i]);}
        }
    }

    private static void B(int[] x, ArrayList<Integer> b) {
        for(int i=0; i<x.length ;i++){
            if(x[i]%5==0){b.add(x[i]);}
        }
    }

    private static void A(int[] x, ArrayList<Integer> a) {
        for(int i=0; i<x.length ;i++){
         if(x[i]%3==0){a.add(x[i]);}
        }
    }
}
